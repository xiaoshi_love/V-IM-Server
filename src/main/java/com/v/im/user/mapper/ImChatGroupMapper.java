package com.v.im.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.v.im.user.entity.ImChatGroup;

/**
 * <p>
 * 群 Mapper 接口
 * </p>
 *
 * @author v
 * @since 2018-10-28
 */
public interface ImChatGroupMapper extends BaseMapper<ImChatGroup> {

}
