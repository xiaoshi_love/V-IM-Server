package com.v.im.user.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 群 前端控制器
 * </p>
 *
 * @author v
 * @since 2018-10-28
 */
@RestController
@RequestMapping("/user/im-chat-group-user")
public class ImChatGroupUserController {

}
